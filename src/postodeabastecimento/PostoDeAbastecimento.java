package postodeabastecimento;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class PostoDeAbastecimento {
    
    static Scanner entrada = new Scanner(new InputStreamReader(System.in));
    
    static ArrayList<Funcionario> funcionarios = new ArrayList();
    static ArrayList<Bomba> bombas = new ArrayList();
    static ArrayList<Consumo> consumos = new ArrayList();
    
    public static void main(String[] args) {
        String cOpcao;
        
        while(true) {
            System.out.print("Cadastrar:\n"
                    + "1 - Funcionário\n"
                    + "2 - Bombas\n"
                    + "3 - Exibir funcionarios\n"
                    + "4 - Exibir bombas\n"
                    + "5 - Cadastrar consumo\n"
                    + "6 - Relatiorios de vendas\n");
            cOpcao = entrada.next();
            
            switch(cOpcao) {
                case "1":
                    CadastrarFuncionarios();
                    break;
                case "2":
                    CadastrarBombas();
                    break;
                case "3":
                    ExibirFuncionarios();
                    break;
                case "4":
                    ExibirBombas();
                    break;
                case "5":
                    CadastrarConsumo();
                    break;
                case "6":
                    RelatorioVendas();
                    break;
                default:
                    return;
            }
        }
    }
    
    static void CadastrarFuncionarios() {
        System.out.print("Informe o numero do funcionário: ");
        String numero = entrada.next();
        System.out.print("Informe o numero do funcionario: ");
        String nome = entrada.next();
        funcionarios.add(new Funcionario(numero, nome));
    }
    
    static void CadastrarBombas() {
        boolean A = false,
                 G = false,
                 D = false;
        
        System.out.print("Informe o numero da bomba: ");
        String numero = entrada.next();
        
        System.out.print("Aceita Alcool? (s/n) ");
        String respA = entrada.next().toUpperCase();
        
        System.out.print("Aceita Gasolina? (s/n) ");
        String respG = entrada.next().toUpperCase();
        
        System.out.print("Aceita Diesel? (s/n) ");
        String respD = entrada.next().toUpperCase();
        
        if (respA.equals("S"))
            A = true;
        if (respG.equals("S"))
            G = true;
        if (respD.equals("S"))
            D = true;
        bombas.add(new Bomba(numero ,A, G, D));
    }
    
    static void ExibirFuncionarios() {
        for(int i = 0; i < funcionarios.size(); i++) {
            System.out.println(funcionarios.get(i).getNumero() + " - " + funcionarios.get(i).getNome());
        }
    }
    
    static void ExibirBombas() {
        for(int i = 0; i < bombas.size(); i++) {
            System.out.println("-----------------------------------");
            System.out.println("Numero: " + bombas.get(i).getNumero()
                                + "\nAceita Alcool: " + bombas.get(i).getAceitaAlcool()
                                + "\nAceita Gasolina: " + bombas.get(i).getAceitaGasolina()
                                + "\nAceita Diesel: " + bombas.get(i).getAceitaDiesel());
        }
    }
    
    static void CadastrarConsumo() {
        String numeroBomba;
        double litros;
        String numeroFuncionario;
        Bomba bomba = null;
        Funcionario funcionario = null;
        EnumTiposCombustivel tpCombustivel = null;
        
        System.out.print("Informe o numero da bomba: ");
        numeroBomba = entrada.next();
        
        for (int i = 0; i < bombas.size(); i++) {
            if (bombas.get(i).getNumero().equals(numeroBomba)) {
                bomba = bombas.get(i);
                break;
            }
        }
        
        System.out.print("Informe a quantidade em litros de combustivel: ");
                
        litros = entrada.nextDouble();
        
        System.out.print("Informe o número do funcionário: ");
        numeroFuncionario = entrada.next();

        for (int i = 0; i < funcionarios.size(); i++) {
            if (funcionarios.get(i).getNumero().equals(numeroFuncionario)) {
                funcionario = funcionarios.get(i);
                break;
            }
        }
        
        System.out.println("Informe o tipo de combustivel\n"
                + "1 - Alcool\n"
                + "2 - Gasolina\n"
                + "3 - Diesel\n");
        
        String tipo = entrada.next();
        
        switch(tipo) {
            case "1":
                tpCombustivel = EnumTiposCombustivel.ALCOOL;
                break;
            case "2":
                tpCombustivel = EnumTiposCombustivel.GASOLINA;
                break;
            case "3":
                tpCombustivel = EnumTiposCombustivel.DIESEL;
                break;
        }
        
        consumos.add(new Consumo(bomba, funcionario, tpCombustivel, litros, new Date()));
    }
    
    static void RelatorioVendas() {
        for(int i = 0; i < consumos.size(); i++) {
            System.out.println("Data: " + consumos.get(i).getData() +
                                 "\nBomba: " + consumos.get(i).getBomba().getNumero()+
                                 "\nTipo combustivel: " + consumos.get(i).getTipoCombustivel()+
                                 "\nLitros: " + consumos.get(i).getLitros()+
                                 "\nFuncionario numero: " + consumos.get(i).getFuncionario().getNumero()+
                                 "\nFuncionario nome: " + consumos.get(i).getFuncionario().getNome());
        }
    }
}
