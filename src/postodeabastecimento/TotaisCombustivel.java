package postodeabastecimento;

public abstract class TotaisCombustivel {
    private static double totalAlcool;
    private static double totalGasolina;
    private static double totalDiesel;
    
    public static void atualizarTotalAlcool(double ltAlcool) {
        totalAlcool += ltAlcool;
    }

    public static double getTotalGasolina() {
        return totalGasolina;
    }
    
    public static void atualizarTotalGasolina(double ltGasolina) {
        totalGasolina += ltGasolina;
    }

    public static double getTotalDiesel() {
        return totalDiesel;
    }
    
    public static void atualizarTotalDiesel(double ltDiesel) {
        totalDiesel += ltDiesel;
    }
    
    public static double getTotalAlcool() {
        return totalAlcool;
    }
}
