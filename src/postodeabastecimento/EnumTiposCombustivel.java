package postodeabastecimento;

public enum EnumTiposCombustivel {
    ALCOOL,
    GASOLINA,
    DIESEL;
}